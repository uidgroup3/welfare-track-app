
$(document).ready(function(){
    var name = false;
    var surname = false;
    var cnp = false;
    var line1 = false;
    var line2 = false;

    $("#name-input").focusout(function(){
        if( $("#name-input").val().length < 3){
            $(this).css("background-color", "#d67272");
            name = false;
        }else{
            name = true;
            $(this).css("background-color", "#74b578");
        }
    });

    $("#surname-input").focusout(function(){
        if( $("#surname-input").val().length < 3){
            $(this).css("background-color", "#d67272");
            surname = false;
        }else{
            surname = true;
            $(this).css("background-color", "#74b578");
        }
    });

    $("#cnp-input").focusout(function(){
        if( $("#cnp-input").val().length != 13){
            $(this).css("background-color", "#d67272");
            cnp = false;
        }else{
            cnp = true;
            $(this).css("background-color", "#74b578");
        }
    });

    $("#line-1-input").focusout(function(){
        if( !/\d/.test($("#line-1-input").val())){
            $(this).css("background-color", "#d67272");
            line1 = false;
        }else{
            line1 = true;
            $(this).css("background-color", "#74b578");
        }
    });

    $("#line-2-input").focusout(function(){
        if( !/\d/.test($("#line-2-input").val())){
            $(this).css("background-color", "#d67272");
            line2 = false;
        }else{
            line2 = true;
            $(this).css("background-color", "#74b578");
        }
    });

    $( "#bus-pass-form" ).submit(function( event ) {

        if(btn.text() == 'Confirm')
        {
            if(name && surname && cnp && line1 && line2)
                alert("Confirmed subscription");
            else{
                alert("Check your inputs again");
            }
            
            event.preventDefault();
        }else{
            window.history.back();
            event.preventDefault();
        }
        
    });
});