const i1 = document.getElementById("i1");
const i2 = document.getElementById("i2");
const i3 = document.getElementById("i3");

i1.style.display="none";
i2.style.display="none";
i3.style.display="none";

if (typeof(Storage) !== "undefined") {
	
    var x1=localStorage.getItem("i1");
	if(x1!=null)
		i1.style.display="block";
	
	 var x2=localStorage.getItem("i2");
	if(x2!=null)
		i2.style.display="block";
	
	 var x3=localStorage.getItem("i3");
	if(x3!=null)
		i3.style.display="block";
} else {
    // Sorry! No Web Storage support..
}