
$(document).ready(function(){

    $("#coldrex-container-2").hide();
    $("#aspenter-container-2").hide();
    $("#gingertea-container-2").hide();
    $("#ginko-biloba-container-2").hide();
    $("#theraflu-container-2").hide();
    $("#message-text").hide();

    $("#coldrex-btn").click(function() {
        $(this).parent().hide();
    });

    $("#gingertea-btn").click(function() {
        $(this).parent().hide();
    });

    $("#ginko-btn").click(function() {
        $(this).parent().hide();
    });

    $("#theraflu-btn").click(function() {
        $(this).parent().hide();
    });

    $("#aspenter-btn").click(function() {
        $(this).parent().hide();
    });

    $("#add-btn").click(function() {
        if($("#coldrex-container-2").is(':visible')){

            if($("#aspenter-container-2").is(':visible')){

                if($("#gingertea-container-2").is(':visible')){

                    if($("#ginko-biloba-container-2").is(':visible')){
                        
                        if(!$("#theraflu-container-2").is(':visible')){
                            $("#theraflu-container-2").show();
                        }
                    }else{
                        $("#ginko-biloba-container-2").show();
                    }
                }else{
                    $("#gingertea-container-2").show();
                }
            }else{
                $("#aspenter-container-2").show();
            }
        }else{
            $("#coldrex-container-2").show();
        }
    });

    $("#aspenter-btn-2").click(function() {
        $(this).parent().hide();
    });

    $("#gingertea-btn-2").click(function() {
        $(this).parent().hide();
    });

    $("#ginko-btn-2").click(function() {
        $(this).parent().hide();
    });

    $("#theraflu-btn-2").click(function() {
        $(this).parent().hide();
    });

    $("#coldrex-btn-2").click(function() {
        $(this).parent().hide();
    });

    $("#user-add-btn").click(function(){
        $("#message-text").show();
        setTimeout(
            function() 
            {
                $("#message-text").hide();
                if($("#coldrex-container-2").is(':visible')){

                    if($("#aspenter-container-2").is(':visible')){
        
                        if($("#gingertea-container-2").is(':visible')){
        
                            if($("#ginko-biloba-container-2").is(':visible')){
                                
                                if(!$("#theraflu-container-2").is(':visible')){
                                    $("#theraflu-container-2").show();
                                }
                            }else{
                                $("#ginko-biloba-container-2").show();
                            }
                        }else{
                            $("#gingertea-container-2").show();
                        }
                    }else{
                        $("#aspenter-container-2").show();
                    }
                }else{
                    $("#coldrex-container-2").show();
                }
            }, 3000);
    });
});