
$(document).ready(function(){

    var checkedFields = 0;

    $("#name-input").focusout(function(){
        if( $("#name-input").val().length < 3){
            $(this).css("background-color", "#d67272");
           
        }else{
            checkedFields++;
            $(this).css("background-color", "#74b578");
        }
        
    });

    $("#surname-input").focusout(function(){
        if( $("#surname-input").val().length < 3){
            $(this).css("background-color", "#d67272");
           
        }else{
            checkedFields++;
            $(this).css("background-color", "#74b578");
        }
        
    });

    $("#cnp-input").focusout(function(){
        if( $("#cnp-input").val().length != 13){
            $(this).css("background-color", "#d67272");
            
        }else{
            checkedFields++;
            $(this).css("background-color", "#74b578");
        }
        
    });

    $("#line-1-input").focusout(function(){
        if( !/\d/.test($("#line-1-input").val())){
            $(this).css("background-color", "#d67272");
           
        }else{
            checkedFields++;
            $(this).css("background-color", "#74b578");
        }
        
    });

    $("#line-2-input").focusout(function(){
        if( !/\d/.test($("#line-2-input").val())){
            $(this).css("background-color", "#d67272");
            
        }else{
            checkedFields++;
            $(this).css("background-color", "#74b578");
        }
        
    });

    $( "#bus-pass-form" ).submit(function( event ) {

        if(checkedFields == 5)
            alert("Confirmed subscription");
        else{
            alert("Check your inputs again");
        }

        event.preventDefault();
    });
});