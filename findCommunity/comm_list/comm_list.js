var i1 = document.getElementById('i1');
i1.onclick = function() {
  //alert("here");
  window.location='../comm_details/cmgrchat_details.html';
}

var i2 = document.getElementById('i2');
i2.onclick = function() {
  //alert("here");
  window.location='../comm_details/food_details.html';
}

var i3 = document.getElementById('i3');
i3.onclick = function() {
  //alert("here");
  window.location='../comm_details/red_lemon_details.html';
}

var i4 = document.getElementById('i4');
i4.onclick = function() {
  //alert("here");
  window.location='../comm_details/mental_health_details.html';
}

var searchBtn =  document.getElementById('search-btn');
searchBtn.onclick = function() {
  var inputText = document.getElementById("search-term").value;

  //alert(`${inputText}`);
  switch(inputText) {
    case 'health': 
      i1.style.display = 'none';
      i2.style.display = 'none';
      i3.style.display = 'none';
      i4.style.display = 'block';
      break;
    case 'food':      
      i1.style.display = 'none';
      i3.style.display = 'none';
      i2.style.display = 'block';
      i4.style.display = 'none';
      break;
    case 'social':      
      i1.style.display = 'block'; 
      i2.style.display = 'none';
      i3.style.display = 'block';
      i4.style.display = 'none';
      break;
    default: 
      i1.style.display = 'block'; 
      i2.style.display = 'block';
      i3.style.display = 'block';
      i4.style.display = 'block';
      break;
  }

}

var home = document.querySelector(".fa-home");
home.onclick=function() { 
  window.location = '../../mainPageComponent/index.html';
};
